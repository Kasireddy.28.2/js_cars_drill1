

function detailsOfCarWithId(array,id){
    if(Array.isArray(array)){
        for(let data of array){
            if(data.id===id){
                return `Car ${id} is a ${data.car_year} ${data.car_make} ${data.car_model}`
            };
        };
    }else{
        return undefined;

    }
};

module.exports=detailsOfCarWithId;