
function BMWAndAudiCars(array){
    if(Array.isArray(array)){
        let bmw_audi_cars=[];
        for(let data of array){
            if(data.car_make==="BMW" || data.car_make==="Audi"){
                bmw_audi_cars.push(data.car_model);
            };

        };

        return JSON.stringify(bmw_audi_cars);
    }else{
        return undefined;
    }
};

module.exports=BMWAndAudiCars;