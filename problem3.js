
function sortingCarModelsAlphabetically(array){
    if(Array.isArray(array)){
        let car_models=[];
        for(let data of array){
            car_models.push(data.car_model);
        };
        return car_models.sort();
    }else{
        return undefined;
    }
};

module.exports=sortingCarModelsAlphabetically;

