 function carsOlderthan2000(array){
        if(Array.isArray(array)){
            let cars_older_than_2000=[];
            for(let data of array){
                if(data.car_year<2000){
                    cars_older_than_2000.push(data.car_model);

                };
            };
            return cars_older_than_2000;
        }else{
            return undefined;
        }
 };

 module.exports=carsOlderthan2000;