

function carYears(array){
    if(Array.isArray(array)){
        let car_years={};
        for(let data of array){
            car_years[data.car_model]=data.car_year;
            
        };
        return car_years;
    }else{
        return undefined;
    }
};


module.exports=carYears;