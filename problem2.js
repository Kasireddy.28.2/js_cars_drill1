
function detailsOfLastCar(array){
    if(Array.isArray(array)){
    return `Last car is a ${array[array.length-1].car_make} ${array[array.length-1].car_model}`;
    }else{
        return undefined;
    }
};

module.exports=detailsOfLastCar;